// hora actual en segundos
setInterval(() => {
    const now = new Date();
    const seconds = now.getSeconds();
    const hours =  now.getHours();
    const minutes = now.getMinutes();

    document.getElementById('hours').textContent = hours;
    document.getElementById('minutes').textContent = minutes;
    document.getElementById('seconds').textContent = seconds;
  }, 1000);
  
  // área de un triángulo
  document.getElementById('calcular').addEventListener('click', () => {
    const base = parseFloat(document.getElementById('base').value);
    const altura = parseFloat(document.getElementById('altura').value);
    const area = (base * altura) / 2;
    document.getElementById('area').textContent = area;
  });
  
  // Calcular raíz cuadrada de un número impar

  const botonRaiz = document.getElementById('raiz');
  botonRaiz.addEventListener('click', calcularRaiz);
  
  function calcularRaiz() {
    const numeroInput = document.getElementById('numero');
    const numero = parseInt(numeroInput.value);
  
    if (isNaN(numero) || numeroInput.value.trim() === '') {
      alert('Ingrese un número válido');
      return;
    }
  
    if (numero % 2 == 1) {
      const raiz = Math.sqrt(numero).toFixed(3);
      const resultadoElemento = document.getElementById('raiz_resultado');
      resultadoElemento.textContent = raiz;
    } else {
      alert('El número ingresado no es impar');
    }
  }
  
    
    //longitud de una cadena de texto
    document.getElementById('longitud').addEventListener('click', () => {
    const cadena = document.getElementById('cadena').value;
    const longitud = cadena.length;
    document.getElementById('longitud_resultado').textContent = longitud;
    });
    
    // Concatenar arrays
    document.getElementById('concatenar').addEventListener('click', () => {
    const array1 = ['Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes'];
    const array2 = ['Sábado', 'Domingo'];
    const concatenacion = array1.concat(array2);
    document.getElementById('concatenacion').textContent = concatenacion.join(', ');
    });
    
    // Mostrar versión del navegador
    const version = navigator.userAgent;
    document.getElementById('version').textContent = version;
    
    // Mostrar ancho y altura de la pantalla
    document.addEventListener("DOMContentLoaded", function() {
      const ancho = window.innerWidth;
      const altura = window.innerHeight;
      document.getElementById('ancho').textContent = ancho;
      document.getElementById('altura').textContent = altura;
  });
  
  
    // Imprimir página
    document.getElementById('imprimir').addEventListener('click', () => {
    window.print();
    });